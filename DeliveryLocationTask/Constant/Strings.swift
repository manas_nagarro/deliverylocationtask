//
//  Strings.swift
//  DeliveryLocationTask
//
//  Created by Manas Bajpai on 22/04/19.
//  Copyright © 2019 Manas Bajpai. All rights reserved.
//

import Foundation

struct ConstantString {
    private init() {}
    static let deliveryListTitle = "Things to deliver"
    static let deliveryDetailTitle = "Delivery Details"
    static let pullToRefreshText = "Pull down to refresh"
    // multiline Message
    static let noDataMessage =  #"""
                                No Data Available.
                                Please pull down to refresh.
                                """#
}
