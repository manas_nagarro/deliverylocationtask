//
//  ServiceConstant.swift
//  DeliveryLocationTask
//
//  Created by Manas Bajpai on 07/05/19.
//  Copyright © 2019 Manas Bajpai. All rights reserved.
//

import Foundation

struct APIEndPoint {
    private init() {}
    static let baseUrl = "https://mock-api-mobile.dev.lalamove.com/"
    static let deliveryService = "deliveries"
}

let  apiTimeOutInterval: TimeInterval = 60
let fetchLimit = 20
