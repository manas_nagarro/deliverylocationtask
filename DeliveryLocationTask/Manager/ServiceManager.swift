//
//  ServiceManager.swift
//  DeliveryLocationTask
//
//  Created by Manas Bajpai on 23/04/19.
//  Copyright © 2019 Manas Bajpai. All rights reserved.
//

import UIKit
import Alamofire

class ServiceManager: NSObject {
    
    static let sharedInstance = ServiceManager()
    
    private override init() {}
    
    func performRequest<T: Decodable>(request: URLRequest, decoder: JSONDecoder = JSONDecoder(), completion: @escaping (Result<T, Error>) -> Void) -> DataRequest {
        return AF.request(request)
            .responseDecodable (decoder: decoder) { (response: DataResponse<T>) in
                completion(response.result)
        }
    }
}
