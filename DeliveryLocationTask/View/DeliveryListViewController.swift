//
//  DeliveryListViewController.swift
//  DeliveryLocationTask
//
//  Created by Manas Bajpai on 22/04/19.
//  Copyright © 2019 Manas Bajpai. All rights reserved.
//

import UIKit
import IHProgressHUD

class DeliveryListViewController: UIViewController {
    
    // MARK: Constants
    let deliveryListTableView = UITableView()
    let noDataMessageLabel = UILabel()
    let deliveryListDataSource = DeliveryListDataSource()
    let error: DynamicValue<ErrorValue> = DynamicValue(ErrorValue())
    let cellIdentifier = "DeliveryItemCell"
    
    lazy var deliveryViewModel: DeliveryViewModel = {
        let deliveryViewModel = DeliveryViewModel(dataSource: deliveryListDataSource, error: error)
        return deliveryViewModel
    }()
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControl.Event.valueChanged)
        let attributedString = NSAttributedString(string: ConstantString.pullToRefreshText)
        refreshControl.attributedTitle = attributedString
        refreshControl.tintColor = UIColor.gray
        return refreshControl
    }()
    
    // MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadUI()
        setUpObservers()
    }
}

// MARK: Helper Methods
extension DeliveryListViewController {
    
    private func loadUI() {
        view.backgroundColor = UIColor.white
        self.title = ConstantString.deliveryListTitle
        configureNoDataLabel()
        configureTableView()
    }
    
    private func configureTableView() {
        
        deliveryListTableView.register(DeliveryItemCell.self, forCellReuseIdentifier: cellIdentifier)
        deliveryListTableView.estimatedRowHeight = CGFloat(TableParameters.estimatedHeight)
        deliveryListTableView.rowHeight = UITableView.automaticDimension
        deliveryListTableView.tableFooterView = UIView()
        view.addSubview(deliveryListTableView)
        deliveryListTableView.addSubview(refreshControl)
        deliveryListTableView.fillSuperview()
        deliveryListTableView.dataSource = self.deliveryListDataSource
        deliveryListTableView.delegate = self
        
    }
    
    private func configureNoDataLabel() {
        
        noDataMessageLabel.font = ApplicationFont.systemBold
        noDataMessageLabel.textColor = UIColor.lightGray
        noDataMessageLabel.text = ConstantString.noDataMessage
        noDataMessageLabel.textAlignment = .center
        noDataMessageLabel.translatesAutoresizingMaskIntoConstraints = false
        noDataMessageLabel.numberOfLines = 0
        view.addSubview(noDataMessageLabel)
        
        noDataMessageLabel.widthAnchor.constraint(equalToConstant: noDataLabelWidthConstant).isActive = true
        noDataMessageLabel.anchorCenterSuperview()
        
        noDataMessageLabel.isHidden = true
    }
    
    private func setUpObservers() {
        
        deliveryListDataSource.data.addAndNotify(observer: self) { [weak self] _ in
            self?.deliveryListTableView.reloadData()
            self?.refreshControl.endRefreshing()
            self?.showHideNoResultMessage()
        }
        
        error.addAndNotify(observer: self) { [weak self] _ in
            if let errorMessage = self?.error.value.message {
                Utility.sharedInstance.showErrorWithMessage(message: errorMessage)
            }
            
            self?.refreshControl.endRefreshing()
            self?.deliveryListTableView.tableFooterView = UIView()
            self?.showHideNoResultMessage()
        }
        
        deliveryViewModel.showLoaderForNoData = { showLoader in
            if showLoader {
                IHProgressHUD.show()
            } else {
                IHProgressHUD.dismiss()
            }
        }
        
        deliveryViewModel.showSpinnerForLoadingMoreData = { [weak self] _ in
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self?.deliveryListTableView.bounds.width ?? 0.0, height: CGFloat(TableParameters.footerHeight))
            self?.deliveryListTableView.tableFooterView = spinner
        }
        
        deliveryViewModel.fetchDeliveries(isPullToRefresh: false)
    }
    
    private func openDeliveryDetailViewController(deliveryItem: DeliveryItem) {
        let deliveryDetailViewController = DeliveryDetailViewController()
        deliveryDetailViewController.deliveryDetailViewModel.setValueForDeliveryItem(deliveryItem: deliveryItem)
        navigationController?.pushViewController(deliveryDetailViewController, animated: true)
    }
    
    private func showHideNoResultMessage() {
        view.bringSubviewToFront(noDataMessageLabel)
        noDataMessageLabel.isHidden = !self.deliveryListDataSource.data.value.isEmpty
    }
    
    @objc private func handleRefresh(_ refreshControl: UIRefreshControl) {
        deliveryViewModel.fetchDeliveries(isPullToRefresh: true)
    }
    
}

// MARK: UITableViewDelegate Methods
extension DeliveryListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let deliveryItems = self.deliveryListDataSource.data.value
        openDeliveryDetailViewController(deliveryItem: deliveryItems[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        deliveryViewModel.loadMoreData(index: indexPath.row)
    }
}
