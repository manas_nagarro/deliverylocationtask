//
//  DeliveryItemCell.swift
//  DeliveryLocationTask
//
//  Created by Manas Bajpai on 22/04/19.
//  Copyright © 2019 Manas Bajpai. All rights reserved.
//

import UIKit
import Kingfisher

class DeliveryItemCell: UITableViewCell {
    
    var deliveryIconImageView = UIImageView()
    var deliveryDetailLabel = UILabel()
    
    var deliveryItem: DeliveryItem? {
        didSet {
            
            guard let deliveryItem = deliveryItem else {
                return
            }
            updateUI(deliveryItem: deliveryItem)
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func configureUI() {
        
        // Image
        deliveryIconImageView.contentMode = .scaleAspectFill
        deliveryIconImageView.clipsToBounds = true
        deliveryIconImageView.backgroundColor = UIColor.gray
        deliveryIconImageView.layer.cornerRadius = cornerRadius
        deliveryIconImageView.kf.indicatorType = .activity
        self.addSubview(deliveryIconImageView)
        
        // Label
        deliveryDetailLabel.font = ApplicationFont.avenirNextDemiBold
        deliveryDetailLabel.numberOfLines = 0
        deliveryDetailLabel.translatesAutoresizingMaskIntoConstraints = false
        deliveryDetailLabel.contentMode = .topLeft
        self.addSubview(deliveryDetailLabel)
        
        deliveryIconImageView.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: CGFloat(CellImageParameters.width), heightConstant: CGFloat(CellImageParameters.height))
        
        deliveryDetailLabel.anchor(deliveryIconImageView.topAnchor, left: deliveryIconImageView.rightAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 10, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        
        deliveryDetailLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: CGFloat(CellImageParameters.height)).isActive = true
    }
    
    private func updateUI(deliveryItem: DeliveryItem) {
        // Image
        deliveryIconImageView.kf.indicatorType = .activity
        deliveryIconImageView.kf.setImage(with: URL(string: deliveryItem.imageUrl ?? "" ))
        
        //Label
        deliveryDetailLabel.text = deliveryItem.description
    }
    
}
