//
//  DeliveryDetailViewController.swift
//  DeliveryLocationTask
//
//  Created by Manas Bajpai on 22/04/19.
//  Copyright © 2019 Manas Bajpai. All rights reserved.
//

import UIKit
import MapKit

class DeliveryDetailViewController: UIViewController {
    
    // MARK: Constants
    let mapView = MKMapView()
    let identifier = "Annotation"
    let regionRadius: CLLocationDistance = 1000
    
    // MARK: Variables
    var genericDeliveryItem: DynamicValue<DeliveryItem>?
    var detailView = UIView()
    var deliveryIconImageView = UIImageView()
    var deliveryDetailLabel = UILabel()
    
    lazy var deliveryDetailViewModel: DeliveryDetailViewModel = {
        let deliveryDetailViewModel = DeliveryDetailViewModel(genericDeliveryItem: genericDeliveryItem)
        return deliveryDetailViewModel
    }()
    
    // MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = ConstantString.deliveryDetailTitle
        configureMapView()
        configureDetailView()
        
        genericDeliveryItem?.addAndNotify(observer: self) { [weak self] _ in
            self?.setAnnotationOnMap()
            self?.setDataOnDetailView()
        }
        
        deliveryDetailViewModel.modelViewDelegate = self
        deliveryDetailViewModel.getGenericDeliveryProduct()
        
    }
}

// MARK: Helper Methods
extension DeliveryDetailViewController {
    
    private func configureMapView() {
        mapView.backgroundColor = UIColor.white
        mapView.mapType = MKMapType.standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        mapView.delegate = self
        mapView.center = view.center
        view.addSubview(mapView)
        mapView.fillSuperview()
    }
    
    private func setAnnotationOnMap() {
        let deliveryLocation = CLLocation(latitude: genericDeliveryItem?.value.location?.lat ?? 0.0, longitude: genericDeliveryItem?.value.location?.lng ?? 0.0)
        let deliveryAnnotation = MKPointAnnotation()
        deliveryAnnotation.title = genericDeliveryItem?.value.location?.address
        deliveryAnnotation.coordinate = deliveryLocation.coordinate
        mapView.addAnnotation(deliveryAnnotation)
        centerMapOnLocation(location: deliveryLocation)
    }
    
    private func centerMapOnLocation(location: CLLocation) {
        
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }    
}

// MARK: Configure Detail View
extension DeliveryDetailViewController {
    
    private func configureDetailView() {
        
        //View
        detailView.backgroundColor = UIColor.white
        detailView.layer.cornerRadius = cornerRadius
        view.addSubview(detailView)
        detailView.anchor(nil, left: mapView.leftAnchor, bottom: mapView.bottomAnchor, right: mapView.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: CGFloat(DetailViewParameters.detailViewBottomConstant), rightConstant: 10, widthConstant: 0, heightConstant: CGFloat(DetailViewParameters.detailViewHeightConstant))
        
        // Image
        deliveryIconImageView.contentMode = .scaleAspectFill
        deliveryIconImageView.layer.cornerRadius = cornerRadius
        deliveryIconImageView.clipsToBounds = true
        deliveryIconImageView.backgroundColor = UIColor.gray
        detailView.addSubview(deliveryIconImageView)
        
        // Label
        deliveryDetailLabel.font = ApplicationFont.avenirNextDemiBold
        deliveryDetailLabel.numberOfLines = 0
        deliveryDetailLabel.translatesAutoresizingMaskIntoConstraints = false
        deliveryDetailLabel.contentMode = .topLeft
        detailView.addSubview(deliveryDetailLabel)
        
        deliveryIconImageView.anchor(detailView.topAnchor, left: detailView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 10, rightConstant: 0, widthConstant: CGFloat(DetailViewParameters.imageWidth), heightConstant: CGFloat(DetailViewParameters.imageHeight))
        
        deliveryDetailLabel.anchorCenterYToSuperview(constant: 0)
        deliveryDetailLabel.anchor(nil, left: deliveryIconImageView.rightAnchor, bottom: nil, right: detailView.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        deliveryDetailLabel.heightAnchor.constraint(lessThanOrEqualToConstant: CGFloat(DetailViewParameters.imageHeight)).isActive = true
        
    }
    
    private func setDataOnDetailView() {
        deliveryIconImageView.kf.indicatorType = .activity
        deliveryIconImageView.kf.setImage(with: URL(string: genericDeliveryItem?.value.imageUrl ?? ""))
        deliveryDetailLabel.text = genericDeliveryItem?.value.description
    }
}

// MARK: MKMapViewDelegate
extension DeliveryDetailViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView!.canShowCallout = true
        } else {
            annotationView!.annotation = annotation
        }
        
        return annotationView
    }
}

// MARK: DeliveryDetailViewModelDelegate
extension DeliveryDetailViewController: DeliveryDetailViewModelDelegate {
    func updateUI(genericDeliveryItem: DynamicValue<DeliveryItem>?) {
        self.genericDeliveryItem = genericDeliveryItem
        setAnnotationOnMap()
        setDataOnDetailView()
    }
}
