//
//  DeliveryListDataSource.swift
//  DeliveryLocationTask
//
//  Created by Manas Bajpai on 22/04/19.
//  Copyright © 2019 Manas Bajpai. All rights reserved.
//

import Foundation
import UIKit

class GenericDataSource<T> : NSObject {
    var data: DynamicValue<[T]> = DynamicValue([])
}

let cellIdentifier = "DeliveryItemCell"

class DeliveryListDataSource: GenericDataSource<DeliveryItem>, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! DeliveryItemCell
        cell.selectionStyle = .none
        cell.accessoryType = .disclosureIndicator
        let deliveryItem = self.data.value[indexPath.row]
        cell.deliveryItem = deliveryItem
        
        return cell
    }
    
}
