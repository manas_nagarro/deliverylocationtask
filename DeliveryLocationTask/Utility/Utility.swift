//
//  Utility.swift
//  DeliveryLocationTask
//
//  Created by Manas Bajpai on 23/04/19.
//  Copyright © 2019 Manas Bajpai. All rights reserved.
//

import UIKit
import NotificationBannerSwift

class Utility: NSObject {
    
    static let sharedInstance = Utility()
    
    private override init() {}
    
    func checkValidUrl(urlStr: String) -> Bool {
        guard URL(string: urlStr) != nil else {
            return false
        }
        return true
    }
    
    func showErrorWithMessage(message: String, style: BannerStyle = .warning) {
        let banner = NotificationBanner(title: message, style: style)
        banner.show()
    }
}
