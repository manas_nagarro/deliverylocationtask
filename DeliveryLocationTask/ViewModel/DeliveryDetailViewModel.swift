//
//  DeliveryDetailViewModel.swift
//  DeliveryLocationTask
//
//  Created by Manas Bajpai on 25/04/19.
//  Copyright © 2019 Manas Bajpai. All rights reserved.
//

import UIKit

protocol DeliveryDetailViewModelDelegate: class {
    func updateUI(genericDeliveryItem: DynamicValue<DeliveryItem>?)
}

struct DeliveryDetailViewModel {
    
    var genericDeliveryItem: DynamicValue<DeliveryItem> = DynamicValue(DeliveryItem())
    
    weak var modelViewDelegate: DeliveryDetailViewModelDelegate?
    
    init( genericDeliveryItem: DynamicValue<DeliveryItem>?) {
        self.genericDeliveryItem = genericDeliveryItem ?? DynamicValue(DeliveryItem())
    }
    
    func setValueForDeliveryItem(deliveryItem: DeliveryItem) {
        self.genericDeliveryItem.value = deliveryItem
    }
    
    func getGenericDeliveryProduct() {
        modelViewDelegate?.updateUI(genericDeliveryItem: self.genericDeliveryItem)
    }
}
