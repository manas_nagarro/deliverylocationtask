//
//  DeliveryViewModel.swift
//  DeliveryLocationTask
//
//  Created by Manas Bajpai on 22/04/19.
//  Copyright © 2019 Manas Bajpai. All rights reserved.
//

import Foundation

struct DeliveryViewModel {
    
    weak var deliveryListDataSource: GenericDataSource<DeliveryItem>?
    var error: DynamicValue<ErrorValue>?
    var dataManager: DataManagerProtocol = DataManager()
    
    var showLoaderForNoData: ((Bool) -> Void)?
    var showSpinnerForLoadingMoreData: ((Bool?) -> Void)?
    
    init( dataSource: GenericDataSource<DeliveryItem>?, error: DynamicValue<ErrorValue>?) {
        self.deliveryListDataSource = dataSource
        self.error = error
    }
    
    func fetchDeliveries(isPullToRefresh: Bool) {
        
        if isPullToRefresh {
            fetchDeliveriesForPullToRefresh()
        } else {
            fetchDeliveries()
        }
    }
    
    func loadMoreData(index: Int) {
        if index + 1 == self.deliveryListDataSource?.data.value.count {
            self.showSpinnerForLoadingMoreData?(true)
            self.fetchDeliveries(isPullToRefresh: false)
        }
    }
    
    private func fetchDeliveries() {
        
        if self.deliveryListDataSource?.data.value.isEmpty ?? true {
            self.showLoaderForNoData?(true)
        }
        
        dataManager.fetchDeliveries(offset: deliveryListDataSource?.data.value.count ?? 0, limit: fetchLimit) { (items, error) in
            
            self.showLoaderForNoData?(false)
            guard error == nil else {
                self.error?.value = ErrorValue(message: error?.localizedDescription)
                return
            }
            if let deliveryItems = items {
                if self.deliveryListDataSource?.data.value.isEmpty ?? true {
                    self.deliveryListDataSource?.data.value = deliveryItems
                } else {
                    let finalDeliveryItems = (self.deliveryListDataSource?.data.value)! + deliveryItems
                    self.deliveryListDataSource?.data.value = finalDeliveryItems
                }
            }
            
        }
    }
    
    private func fetchDeliveriesForPullToRefresh() {
        dataManager.fetchDeliveriesForPullToRefresh(offset: 0, limit: fetchLimit) { (items, error) in
            
            guard error == nil else {
                self.error?.value = ErrorValue(message: error?.localizedDescription)
                return
            }
            
            if let deliveryItems = items {
                self.deliveryListDataSource?.data.value = deliveryItems
            }
        }
    }
}
