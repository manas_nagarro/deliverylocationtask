//
//  ErrorValue.swift
//  DeliveryLocationTask
//
//  Created by Manas Bajpai on 07/05/19.
//  Copyright © 2019 Manas Bajpai. All rights reserved.
//

import Foundation

struct ErrorValue: Codable {
   
    var message: String?
    
}
