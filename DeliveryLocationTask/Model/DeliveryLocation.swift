//
//  DeliveryLocation.swift
//  DeliveryLocationTask
//
//  Created by Manas Bajpai on 24/04/19.
//  Copyright © 2019 Manas Bajpai. All rights reserved.
//

import Foundation

struct DeliveryLocation: Codable {
    
    var lat: Double?
    var lng: Double?
    var address: String?
    
}

extension DeliveryLocation {
    enum CodingKeys: String, CodingKey {
        //Encoding/decoding will only include the properties defined in this enum, rest will be ignored
        case lat
        case lng
        case address
    }
}
