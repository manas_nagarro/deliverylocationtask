//
//  DeliveryItem.swift
//  DeliveryLocationTask
//
//  Created by Manas Bajpai on 22/04/19.
//  Copyright © 2019 Manas Bajpai. All rights reserved.
//

import Foundation

struct DeliveryItem: Codable {
    
    var id: Int?
    var description: String?
    var imageUrl: String?
    var location: DeliveryLocation?
    
}

extension DeliveryItem {
    enum CodingKeys: String, CodingKey {
        //Encoding/decoding will only include the properties defined in this enum, rest will be ignored
        case id
        case description
        case imageUrl
        case location
    }
}
