//
//  DeliveryProduct+CoreDataProperties.swift
//  
//
//  Created by Manas Bajpai on 25/04/19.
//
//

import Foundation
import CoreData

extension DeliveryProduct {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DeliveryProduct> {
        return NSFetchRequest<DeliveryProduct>(entityName: "DeliveryProduct")
    }

    @NSManaged public var address: String?
    @NSManaged public var id: Int16
    @NSManaged public var imageUrl: String?
    @NSManaged public var itemDescription: String?
    @NSManaged public var lat: Double
    @NSManaged public var lng: Double

}
