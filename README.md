## DeliveryLocationTask (iOS)
A project which lists items to deliver in a list and show their location and details on map view. 

# Installation
- To run the project :
- Open podfile from project repository 
- Open terminal and cd to the directory containing the Podfile
- Run the "pod install" command
- Open DeliveryLocationTask.xcworkspace 

# Requirement :
Xcode : 10.2

# Supported OS version :
iOS (11.x, 12.x)  

# Language 
Swift 5.0

# Version :
1.0 


# (CocoaPods/Libraries) Used     
- Alamofire
- Kingfisher
- IHProgressHUD
- Firebase/Core
- Fabric
- Crashlytics
- NotificationBannerSwift
- SwiftLint
- OHHTTPStubs/Swift

# Design Pattern Used
## MVVM
MVVM stands for Model, View, ViewModel, a specific architecture where the ViewModel stands between View and Model providing interfaces to imitate UI component. This connection is made by “binding” values, linking logical data to the UI.

![](Images/mvvm.png)

__View:__ This layer interacts with the ViewModel via view model instance. In iOS View layer is represented by View and ViewController.

__ViewModel:__ This layer prepares view model which is used by view for display. It interacts with the DataManager layer to retrieve the data. This layer will notify the changes in data to the View layer using completion handler.

__DataManager:__ This layer is in between ViewModel and Model. DataManager provides data intelligently to the view model. If data is already stored in coredata, it will return that data to view model. If data is not present in core data, it will fetch the data from API and return it back to view model and simultaneously store the data in core data. This layer is doing the conversion of the NSManagedObject model to a service model which is used in the app.

__Model:__ Model store the data from server and local database. Core Data model is for holding local DB data and service model is used for server data. Model is interacting with the data manager layer.

- __CoreDataManager:__ This class interacts with database i.e CoreData. It performs the all database related operations and responds back to DataManager.
- __ServiceManger:__ This class interacts remotely. It retrieves the data from the server and map into the model.
- __DeliveryService:__ This class interacts with ServiceManger to provide modularity and separation of concern. 


# Linting
## SwiftLint
- Integration of SwiftLint into an Xcode scheme to keep a codebase consistent and maintainable .
- Install the swiftLint via cocoaPod and need to add a new "Run Script Phase" with:
"${PODS_ROOT}/SwiftLint/swiftlint"
- .swiftlint.yml file is used for basic set of rules . It is placed inside the project folder.

# MapView
- Used iOS Mapkit framework to show map, hence no key required.

# Data Caching
- Core Data is used for data caching. Items fetched from server are displayed to UI and parallely saved in to coredata
- If data is available in core data then it fetchs data from coredata and displayed on UI.
- __Pull to refresh :__
    1. If network available - fetches data from starting index from server, if data is available then it is displayed to UI and parallely deletes all records from coredata, and then save it.
    2. If network not available - Error message is displayed and data remains the same.

# Firebase Crashlytics
- Create account on firebase.
- Please replace "GoogleService-Info.plist" file with your plist file which will be geretated while creating an app on firebase.
- For more details follow the link https://firebase.google.com/docs/crashlytics/get-started

# Unit Testing
- Unit testing is done by using XCTest. 
- To run tests click Product->Test or (cmd+U)

![](Images/TestReport.png)


# Code Coverage
- Run tests by clicking Product->Test or (cmd+U)
- Open the Report navigator in Xcode’s navigation pane, select the Test report, then select the Coverage tab. Xcode will display the overall code coverage for the framework, and we can expand this to get coverage data on individual files and functions.

![](Images/CodeCoverage.png)


# Assumptions        
-   The app is designed for iPhones only.        
-   App support english language.
-   Mobile platform supported: iOS (11.x, 12.x)        
-   Device support - iPhone 5s, iPhone 6 Series, iPhone SE, iPhone 7 Series, iPhone 8 Series, iPhone X Series    
-   iPhone app support would be limited to portrait mode.
-   Data caching is available, but real time data syncing with server is not yet supported.

# Improvements
-  UI Testing could be implemented.
-  Real time data syncing could be implemented.
-  Localization could be implemented.

# Application Screenshots

![](Images/Screen1.png)

![](Images/Screen2.png)



# License
## MIT License

### Copyright (c) 2019 

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
