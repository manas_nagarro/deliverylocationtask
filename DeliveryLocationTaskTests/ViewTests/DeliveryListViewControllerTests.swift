//
//  DeliveryListViewControllerTests.swift
//  DeliveryLocationTaskTests
//
//  Created by Manas Bajpai on 25/04/19.
//  Copyright © 2019 Manas Bajpai. All rights reserved.
//

import XCTest
@testable import DeliveryLocationTask

class DeliveryListViewControllerTests: XCTestCase {
    
    var deliveryListViewController: DeliveryListViewController!

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let navigationController = AppDelegate.delegate().window?.rootViewController as! UINavigationController
        deliveryListViewController = navigationController.viewControllers.first as? DeliveryListViewController
    }
    
    func testTableViewDelegateConformance() {
        XCTAssertTrue(deliveryListViewController.conforms(to: UITableViewDelegate.self))
    }
    
    func testShowNoResultMessage () {
        if deliveryListViewController.deliveryViewModel.deliveryListDataSource?.data.value.isEmpty ?? true {
            XCTAssertFalse(deliveryListViewController.noDataMessageLabel.isHidden)
        }
    }
    
    func testRequiredElementShouldNotNil() {
        XCTAssertNotNil(deliveryListViewController.title)
        XCTAssertNotNil(deliveryListViewController.deliveryListTableView)
        XCTAssertNotNil(deliveryListViewController.noDataMessageLabel)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
}
