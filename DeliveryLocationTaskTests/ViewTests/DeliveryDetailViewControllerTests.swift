//
//  DeliveryDetailViewControllerTests.swift
//  DeliveryLocationTaskTests
//
//  Created by Manas Bajpai on 25/04/19.
//  Copyright © 2019 Manas Bajpai. All rights reserved.
//

import XCTest
import MapKit
@testable import DeliveryLocationTask

class DeliveryDetailViewControllerTests: XCTestCase {
    
    var deliveryDetailVC = DeliveryDetailViewController()
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class
        deliveryDetailVC.deliveryDetailViewModel.setValueForDeliveryItem(deliveryItem: DeliveryItem())
        deliveryDetailVC.viewDidLoad()
    }
    
    func testMapViewDelegateConformance() {
        XCTAssertTrue(deliveryDetailVC.conforms(to: MKMapViewDelegate.self))
    }
    
    func testNotNilElements() {
        XCTAssertNotNil(deliveryDetailVC.title)
        XCTAssertNotNil(deliveryDetailVC.deliveryDetailViewModel)
        XCTAssertNotNil(deliveryDetailVC.deliveryDetailViewModel.genericDeliveryItem.value)
        XCTAssertNotNil(deliveryDetailVC.mapView)
        XCTAssertNotNil(deliveryDetailVC.detailView)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
}
