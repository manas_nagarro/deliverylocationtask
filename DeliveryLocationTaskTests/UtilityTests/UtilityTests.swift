//
//  UtilityTests.swift
//  DeliveryLocationTaskTests
//
//  Created by Manas Bajpai on 24/04/19.
//  Copyright © 2019 Manas Bajpai. All rights reserved.
//

import XCTest
@testable import DeliveryLocationTask

class UtilityTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func testQueryParametersAddedSuccessfully() {
        let endpoint = APIEndPoint.baseUrl + APIEndPoint.deliveryService
        let url = URL(string: endpoint)!
        let finalURL = url.appending("offset", value: String(0)).appending("limit", value: String(20))
        let expectedUrlString = endpoint + "?offset=0&limit=20"
        XCTAssertEqual(finalURL.absoluteString, expectedUrlString, "Query parameters not added successfully")
    }
    
    func testUrlValidator() {
        let endpoint = APIEndPoint.baseUrl + APIEndPoint.deliveryService
        let finalUrlString = endpoint + "?offset=0&limit=20"
        XCTAssertTrue(Utility.sharedInstance.checkValidUrl(urlStr: finalUrlString))
    }
    
    func testInvalidUrlValidator() {
        let endpoint = APIEndPoint.baseUrl + APIEndPoint.deliveryService
        let finalUrlString = endpoint + "?offset =0&limit=20"
        XCTAssertFalse(Utility.sharedInstance.checkValidUrl(urlStr: finalUrlString))
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
}
