//
//  DeliveryViewModelTests.swift
//  DeliveryLocationTaskTests
//
//  Created by Manas Bajpai on 07/05/19.
//  Copyright © 2019 Manas Bajpai. All rights reserved.
//

import XCTest
@testable import DeliveryLocationTask

class DeliveryViewModelTests: XCTestCase {
    
    let deliveryListDataSource = DeliveryListDataSource()
    let error: DynamicValue<ErrorValue> = DynamicValue(ErrorValue())
    var deliveryViewModel: DeliveryViewModel?
    
    override func setUp() {
        deliveryViewModel = DeliveryViewModel(dataSource: deliveryListDataSource, error: error)
    }
    
    func testFetchDeliveriesWithSuccessForNoData() {
        let testSuccessDataManager = TestSuccessDataManagerForBlankItems()
        deliveryViewModel?.dataManager = testSuccessDataManager
        deliveryViewModel?.fetchDeliveries(isPullToRefresh: false)
        XCTAssertEqual(deliveryViewModel?.deliveryListDataSource?.data.value.count, 0)
    }
    
    func testFetchDeliveriesWithSuccessForValidData() {
        let testSuccessDataManager = TestSuccessDataManager()
        deliveryViewModel?.dataManager = testSuccessDataManager
        deliveryViewModel?.fetchDeliveries(isPullToRefresh: false)
        XCTAssertEqual(deliveryViewModel?.deliveryListDataSource?.data.value.count, 1)
    }
    
    func testFetchDeliveriesWithError() {
        let testFailureDataManager = TestFailureDataManager()
        deliveryViewModel?.dataManager = testFailureDataManager
        deliveryViewModel?.fetchDeliveries(isPullToRefresh: false)
        XCTAssertEqual(deliveryViewModel?.error?.value.message, "Mock error")
    }
    
    func testFetchDeliveriesForPullToRefreshWithSuccessForNoData() {
        let testSuccessDataManager = TestSuccessDataManagerForBlankItems()
        deliveryViewModel?.dataManager = testSuccessDataManager
        deliveryViewModel?.fetchDeliveries(isPullToRefresh: true)
        XCTAssertEqual(deliveryViewModel?.deliveryListDataSource?.data.value.count, 0)
    }
    
    func testFetchDeliveriesForPullToRefreshWithSuccessForValidData() {
        let testSuccessDataManager = TestSuccessDataManager()
        deliveryViewModel?.dataManager = testSuccessDataManager
        deliveryViewModel?.fetchDeliveries(isPullToRefresh: true)
        XCTAssertEqual(deliveryViewModel?.deliveryListDataSource?.data.value.count, 1)
    }
    
    func testFetchDeliveriesForPullToRefreshWithError() {
        let testFailureDataManager = TestFailureDataManager()
        deliveryViewModel?.dataManager = testFailureDataManager
        deliveryViewModel?.fetchDeliveries(isPullToRefresh: true)
        XCTAssertEqual(deliveryViewModel?.error?.value.message, "Mock error for pull refresh")
    }
    
    func testLoadMoreDataWithSuccess() {
        deliveryViewModel = DeliveryViewModel(dataSource: deliveryListDataSource, error: error)
        let testSuccessDataManager = TestSuccessDataManager()
        deliveryViewModel?.dataManager = testSuccessDataManager
        deliveryViewModel?.deliveryListDataSource?.data.value = [DeliveryItem()]
        deliveryViewModel?.loadMoreData(index: 0)
        XCTAssertEqual(deliveryViewModel?.deliveryListDataSource?.data.value.count, 2)
    }
    
    override func tearDown() {
        
    }
}
