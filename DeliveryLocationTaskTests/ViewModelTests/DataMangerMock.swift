//
//  DataMangerMock.swift
//  DeliveryLocationTaskTests
//
//  Created by Manas Bajpai on 08/05/19.
//  Copyright © 2019 Manas Bajpai. All rights reserved.
//

import Foundation
@testable import DeliveryLocationTask

class TestFailureDataManager: DataManagerProtocol {
    
    func fetchDeliveries(offset: Int, limit: Int, completion: @escaping ([DeliveryItem]?, Error?) -> Void) {
        let error = NSError(domain: "ERROR", code: 404, userInfo: [NSLocalizedDescriptionKey: "Mock error"])
        completion(nil, error)
    }
    
    func fetchDeliveriesForPullToRefresh(offset: Int, limit: Int, completion: @escaping ([DeliveryItem]?, Error?) -> Void) {
        let error = NSError(domain: "ERROR", code: 404, userInfo: [NSLocalizedDescriptionKey: "Mock error for pull refresh"])
        completion(nil, error)
    }
}

class TestSuccessDataManager: DataManagerProtocol {
    
    func fetchDeliveries(offset: Int, limit: Int, completion: @escaping ([DeliveryItem]?, Error?) -> Void) {
        completion([DeliveryItem()], nil)
    }
    
    func fetchDeliveriesForPullToRefresh(offset: Int, limit: Int, completion: @escaping ([DeliveryItem]?, Error?) -> Void) {
        completion([DeliveryItem()], nil)
    }
}

class TestSuccessDataManagerForBlankItems: DataManagerProtocol {
    
    func fetchDeliveries(offset: Int, limit: Int, completion: @escaping ([DeliveryItem]?, Error?) -> Void) {
        completion([], nil)
    }
    
    func fetchDeliveriesForPullToRefresh(offset: Int, limit: Int, completion: @escaping ([DeliveryItem]?, Error?) -> Void) {
        completion([], nil)
    }
}
