//
//  ServiceManagerTests.swift
//  DeliveryLocationTaskTests
//
//  Created by Manas Bajpai on 25/04/19.
//  Copyright © 2019 Manas Bajpai. All rights reserved.
//

import XCTest
import Alamofire
@testable import DeliveryLocationTask

class ServiceManagerTests: XCTestCase {
    
    let host = "mock-api-mobile.dev.lalamove.com"
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func testValidDeliveryResponse() {
        testDeliveryValidResponseForURL(host: host, fileName: "SuccessValid.json")
    }
    
    func testInValidDeliveryResponse() {
        testDeliveryInValidResponseForURL(host: host, fileName: "Failure.json")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
}

extension ServiceManagerTests {
    
    func testDeliveryValidResponseForURL(host: String, fileName: String) {
        
        XCHttpStub.request(withPathRegex: host, withResponseFile: fileName)
        let resultExpectation = expectation(description: "Invalid Json")
        DeliveryService.shared.fetchDeliveries(offset: 0, limit: 20) { result in
            
            switch result {
            case .success(let deliveryItems):
                XCTAssertNotNil(deliveryItems, "Expectation fullfilled")
                XCTAssertEqual(20, deliveryItems.count)
                resultExpectation.fulfill()
                
            case .failure(let error):
                XCTAssertNotNil(error.localizedDescription, "Expectation fulfilled with error")
                resultExpectation.fulfill()
                debugPrint(error)
            }
        }
        
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTAssertNotNil(error, "Request timed out")
            }
        }
        
    }
    
    func testDeliveryInValidResponseForURL(host: String, fileName: String) {
        
        XCHttpStub.request(withPathRegex: host, withResponseFile: fileName)
        let resultExpectation = expectation(description: "Expected delivery Items")
        DeliveryService.shared.fetchDeliveries(offset: 0, limit: 20) { result in
            
            switch result {
            case .success(let deliveryItems):
                XCTAssertNotNil(deliveryItems, "Expectation failed - No delivery Items from Invalid json")
                XCTAssertEqual(20, deliveryItems.count)
                resultExpectation.fulfill()
                
            case .failure(let error):
                XCTAssertNotNil(error.localizedDescription, "Invalid Json")
                resultExpectation.fulfill()
                debugPrint(error)
            }
        }
        
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTAssertNotNil(error, "Request timed out")
            }
        }
        
    }
}
