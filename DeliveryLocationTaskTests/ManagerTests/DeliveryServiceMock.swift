//
//  DeliveryServiceMock.swift
//  DeliveryLocationTaskTests
//
//  Created by Manas Bajpai on 08/05/19.
//  Copyright © 2019 Manas Bajpai. All rights reserved.
//

import Foundation
@testable import DeliveryLocationTask

class TestDeliveryServiceForEmptyResponse: DeliveryServiceProtocol {
    
    func fetchDeliveries(offset: Int, limit: Int, _ completion: @escaping ((Result<[DeliveryItem], Error>) -> Void)) {
        completion(.success([]))
    }
}

class TestDeliveryServiceForValidResponse: DeliveryServiceProtocol {
    
    func fetchDeliveries(offset: Int, limit: Int, _ completion: @escaping ((Result<[DeliveryItem], Error>) -> Void)) {
        let testRecord = createMockDeliveryItem(id: 1)
        completion(.success([testRecord]))
    }
    
    private func createMockDeliveryItem(id: Int) -> DeliveryItem {
        
        let deliveryLocation = DeliveryLocation(lat: 28.459497, lng: 77.026634, address: "Gurgaon")
        let deliveryItem = DeliveryItem(id: id, description: "Deliver to Luke", imageUrl: "https://s3-ap-southeast-1.amazonaws.com/lalamove-mock-api/images/pet-8.jpeg", location: deliveryLocation)
        return deliveryItem
        
    }
}

class TestDeliveryServiceForFailure: DeliveryServiceProtocol {
    
    func fetchDeliveries(offset: Int, limit: Int, _ completion: @escaping ((Result<[DeliveryItem], Error>) -> Void)) {
        let error = NSError(domain: "ERROR", code: 404, userInfo: [NSLocalizedDescriptionKey: "Mock error"])
        completion(.failure(error))
    }
}
