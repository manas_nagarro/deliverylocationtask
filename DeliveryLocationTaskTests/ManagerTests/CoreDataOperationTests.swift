//
//  CoreDataOperationTests.swift
//  DeliveryLocationTaskTests
//
//  Created by Manas Bajpai on 24/04/19.
//  Copyright © 2019 Manas Bajpai. All rights reserved.
//

import XCTest
import CoreData
@testable import DeliveryLocationTask

class CoreDataOperationTests: XCTestCase {
    
    var coreDataManager: CoreDataManager?
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        let managedObjectModel = NSManagedObjectModel.mergedModel(from: [Bundle.main] )!
        return managedObjectModel
    }()
    
    lazy var mockPersistantContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "DeliveryLocationTask", managedObjectModel: self.managedObjectModel)
        let description = NSPersistentStoreDescription()
        description.type = NSInMemoryStoreType
        description.shouldAddStoreAsynchronously = false // Make it simpler in test env
        
        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores { (description, error) in
            // Check if the data store is in memory
            precondition( description.type == NSInMemoryStoreType )
            
            // Check if creating container wrong
            if let error = error {
                fatalError("Create an in-memory coordinator failed \(error)")
            }
        }
        return container
    }()
    
    override func setUp() {
        coreDataManager = CoreDataManager.sharedInstance
        coreDataManager?.persistentContainer = mockPersistantContainer
        initStubs()
    }
    
    func testDeliveryProductCount() {
        let deliveryProducts = coreDataManager?.fetchDeliveries(offset: 0, limit: 5)
        XCTAssertEqual(deliveryProducts?.count, 5)
    }
    
    func testDeliveryProductInsertion() {
        
        let mockDeliveryItem = createMockDeliveryItem(id: 1)
        let deliveryProduct = coreDataManager?.insert(deliveryItem: mockDeliveryItem)
        XCTAssertNotNil(deliveryProduct)
    }
    
    func testSave() {
        
        _ = expectation(forNotification: NSNotification.Name(rawValue: Notification.Name.NSManagedObjectContextDidSave.rawValue), object: nil, handler: nil)
        
        let mockDeliveryItem = createMockDeliveryItem(id: 1)
        _ = coreDataManager?.insert(deliveryItem: mockDeliveryItem)
        
        //Assert save is called via notification (wait)
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    override func tearDown() {
        flushData()
    }
}

// MARK: Helper Methods
extension CoreDataOperationTests {
    
    func initStubs() {
        
        let mockDeliveryItem1 = createMockDeliveryItem(id: 1)
        _ = coreDataManager?.insert(deliveryItem: mockDeliveryItem1)
        let mockDeliveryItem2 = createMockDeliveryItem(id: 2)
        _ = coreDataManager?.insert(deliveryItem: mockDeliveryItem2)
        let mockDeliveryItem3 = createMockDeliveryItem(id: 3)
        _ = coreDataManager?.insert(deliveryItem: mockDeliveryItem3)
        let mockDeliveryItem4 = createMockDeliveryItem(id: 4)
        _ = coreDataManager?.insert(deliveryItem: mockDeliveryItem4)
        let mockDeliveryItem5 = createMockDeliveryItem(id: 5)
        _ = coreDataManager?.insert(deliveryItem: mockDeliveryItem5)
    }
    
    func flushData() {
        
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest<NSFetchRequestResult>(entityName: "DeliveryProduct")
        let objs = try? mockPersistantContainer.viewContext.fetch(fetchRequest)
        if let objs = objs {
            for case let obj as NSManagedObject in objs {
                mockPersistantContainer.viewContext.delete(obj)
            }
            do {
                try mockPersistantContainer.viewContext.save()
            } catch {
                debugPrint("Save context fail \(error)")
            }
        }
    }
    
    func createMockDeliveryItem(id: Int) -> DeliveryItem {
        
        let deliveryLocation = DeliveryLocation(lat: 28.459497, lng: 77.026634, address: "Gurgaon")
        let deliveryItem = DeliveryItem(id: id, description: "Deliver to Luke", imageUrl: "https://s3-ap-southeast-1.amazonaws.com/lalamove-mock-api/images/pet-8.jpeg", location: deliveryLocation)
        return deliveryItem
        
    }
    
}
