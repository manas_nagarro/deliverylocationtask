//
//  DataManagerTests.swift
//  DeliveryLocationTaskTests
//
//  Created by Manas Bajpai on 07/05/19.
//  Copyright © 2019 Manas Bajpai. All rights reserved.
//

import XCTest
@testable import DeliveryLocationTask

class DataManagerTests: XCTestCase {
    
    var dataManager = DataManager()
    var testCoreDataManager: TestCoreDataManager!
    
    override func setUp() {
        // get
        testCoreDataManager = TestCoreDataManager()
        
        // Set
        dataManager.coreDataManager = testCoreDataManager
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        testCoreDataManager = nil
    }
    
    func testFetchDeliveriesWithValidLocalData() {
        let testDeliveryServiceForValidResponse = TestDeliveryServiceForValidResponse()
        dataManager.deliveryService = testDeliveryServiceForValidResponse
        dataManager.fetchDeliveries(offset: 0, limit: fetchLimit) { (deliveryItems, error) in
            let count: Int = deliveryItems?.count ?? 0
            XCTAssertGreaterThan(count, 0)
        }
    }
    
    func testFetchDeliveriesWithNoLocalData() {
        let testDeliveryServiceForEmptyResponse = TestDeliveryServiceForEmptyResponse()
        dataManager.deliveryService = testDeliveryServiceForEmptyResponse
        testCoreDataManager.isEmptyResponse = true
        dataManager.fetchDeliveries(offset: 0, limit: fetchLimit) { (deliveryItems, error) in
            let count: Int = deliveryItems?.count ?? 0
            XCTAssertEqual(count, 0)
        }
    }
    
    func testFetchDeliveriesFromServerWithSuccess() {
        let testDeliveryServiceForEmptyResponse = TestDeliveryServiceForValidResponse()
        dataManager.deliveryService = testDeliveryServiceForEmptyResponse
        testCoreDataManager.isEmptyResponse = true
        dataManager.fetchDeliveries(offset: 0, limit: fetchLimit) { (deliveryItems, error) in
            XCTAssertNotNil(deliveryItems)
        }
    }
    
    func testFetchDeliveriesFromServerWithFailure() {
        let testDeliveryServiceForFailure = TestDeliveryServiceForFailure()
        dataManager.deliveryService = testDeliveryServiceForFailure
        testCoreDataManager.isEmptyResponse = true
        dataManager.fetchDeliveries(offset: 0, limit: fetchLimit) { (deliveryItems, error) in
            XCTAssertNil(deliveryItems)
            XCTAssertNotNil(error)
        }
    }
    
    func testFetchDeliveriesForPullToRefreshWithSuccess() {
        let testDeliveryServiceForEmptyResponse = TestDeliveryServiceForValidResponse()
        dataManager.deliveryService = testDeliveryServiceForEmptyResponse
        dataManager.fetchDeliveriesForPullToRefresh(offset: 0, limit: fetchLimit) { (deliveryItems, error) in
            XCTAssertNotNil(deliveryItems)
        }
    }
    
    func testFetchDeliveriesForPullToRefreshWithFailure() {
        let testDeliveryServiceForFailure = TestDeliveryServiceForFailure()
        dataManager.deliveryService = testDeliveryServiceForFailure
        testCoreDataManager.isEmptyResponse = true
        dataManager.fetchDeliveriesForPullToRefresh(offset: 0, limit: fetchLimit) { (deliveryItems, error) in
            XCTAssertNil(deliveryItems)
            XCTAssertNotNil(error)
        }
    }
}
