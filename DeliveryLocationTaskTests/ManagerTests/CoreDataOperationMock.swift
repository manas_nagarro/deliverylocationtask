//
//  CoreDataOperationMock.swift
//  DeliveryLocationTaskTests
//
//  Created by Manas Bajpai on 08/05/19.
//  Copyright © 2019 Manas Bajpai. All rights reserved.
//

import Foundation
@testable import DeliveryLocationTask

class TestCoreDataManager: CoreDataManagerProtocol {
    var isEmptyResponse: Bool = false
    var isDeleteAllCalled: Bool = false
    var isInsertCalled: Bool = false
    
    func insert(deliveryItem: DeliveryItem) -> DeliveryProduct? {
        isInsertCalled = true
        return nil
    }
    
    func deleteAllItems() {
        isDeleteAllCalled = true
    }
    
    func fetchDeliveries(offset: Int, limit: Int) -> [DeliveryProduct]? {
        if isEmptyResponse == true {
            return []
        }
        
        let testRecord = DeliveryProduct(context: CoreDataManager.sharedInstance.persistentContainer.viewContext)
        return [testRecord]
    }
}
